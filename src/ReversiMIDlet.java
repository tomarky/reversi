import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;

public final class ReversiMIDlet extends MIDlet implements CommandListener
{
	Thread thread = null;
	Main main = null;
	
	Command exitCommand = null;
	Command newCommand = null;
	
	public ReversiMIDlet()
	{
		exitCommand = new Command("EXIT", Command.EXIT, 0);
		newCommand = new Command("NEW", Command.SCREEN, 0);
		
		main = new Main();
		main.addCommand(exitCommand);
		main.addCommand(newCommand);
		main.setCommandListener(this);
		Display.getDisplay(this).setCurrent(main);
	}
	
	public void destroyApp(boolean unconditional) throws MIDletStateChangeException
	{
		// no code
	}
	
	public void pauseApp() {}
	
	public void startApp() throws MIDletStateChangeException
	{
		if (thread == null && main != null)
		{
			thread = new Thread(main);
			thread.start();
		}
	}
	
	public void commandAction(Command cmd, Displayable disp)
	{
		if (cmd == exitCommand)
		{
			notifyDestroyed();
		}
		else if (cmd == newCommand)
		{
			main.requestNewGame();
		}
	}
	
}