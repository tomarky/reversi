import java.util.Random;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.GameCanvas;

final class Main extends GameCanvas implements Runnable
{
	
	final boolean[] cpu;
	final int[][] field;
	final boolean[] putable;
	int black, white, turn;
	final Random rand;
	
	boolean flagNewGame;

	Main()
	{
		super(false);
		
		cpu = new boolean[3];
		field = new int[8][8];
		putable = new boolean[64];
		flagNewGame = true;
		
		rand = new Random();
	}
	
	void initGame()
	{
		for (int row = 0; row < 8; row++)
		{
			for (int col = 0; col < 8; col++)
			{
				field[row][col] = 0;
			}
		}
		field[3][3] = field[4][4] = 1;
		field[3][4] = field[4][3] = 2;
		cpu[1] = false;
		cpu[2] = true;
		black = 2;
		white = 2;
		turn = 1;
	}
	
	void drawBase(Graphics g)
	{
		g.setColor(0, 127, 0);
		g.fillRect(0, 0, 240, 268);
		
		g.setColor(0, 0, 0);
		g.fillArc(0, 0, 25, 25, 0, 360);
		
		g.setColor(255, 255, 255);
		g.fillArc(215, 0, 25, 25, 0, 360);
		
		g.drawLine(0, 27, 240, 27);
		for (int row = 0; row < 8; row++)
		{
			int y = row * 30 + 28;
			for (int col = 0; col < 8; col++)
			{
				int x = col * 30;
				g.drawRect(x, y, 29, 29);
			}
		}
	}
	
	void drawNames(Graphics g)
	{
		g.setColor(0, 127, 0);
		g.fillRect(26, 0, 60, 29);
		g.fillRect(154, 0, 60, 29);
	
		g.setColor(255, 255, 255);
		g.drawString(cpu[1] ? "�b�n�l" : "���Ȃ�", 26, 4, Graphics.LEFT | Graphics.TOP);
		g.drawString(cpu[2] ? "�b�n�l" : "���Ȃ�", 214, 4, Graphics.RIGHT | Graphics.TOP);
	}
	
	void drawScores(Graphics g)
	{
		g.setColor(0, 127, 0);
		g.fillRect(95, 0, 50, 26);
		
		String msg = (black < 10 ? " " : "") + Integer.toString(black)
				   + "-" + Integer.toString(white) + (white < 10 ? " " : "");
		
		g.setColor(255, 255, 255);
		g.drawString(msg, 120, 4, Graphics.HCENTER | Graphics.TOP);
	}
	
	void drawAllStones(Graphics g)
	{
		g.drawLine(0, 27, 240, 27);
		for (int row = 0; row < 8; row++)
		{
			int y = row * 30 + 28;
			for (int col = 0; col < 8; col++)
			{
				int x = col * 30;
				g.drawRect(x, y, 29, 29);
				switch (field[row][col])
				{
				case 1:
					g.setColor(0, 0, 0);
					g.fillArc(x + 1, y + 1, 27, 27, 0, 360);
					g.setColor(255, 255, 255);
					break;
				case 2:
					g.fillArc(x + 1, y + 1, 27, 27, 0, 360);
					break;
				}
			}
		}
	}
	
	void clearPutable(Graphics g)
	{
		for (int i = 0; i < 64; i++)
		{
			if (putable[i])
			{
				clearMass(g, i >> 3, i & 7);
			}
		}
	}
	int findTarget(int y, int x, int dy, int dx, int target, int stopper)
	{
		int ty = y + dy, tx = x + dx;
		while (0 <= ty && ty < 8 && 0 <= tx && tx < 8)
		{
			if (field[ty][tx] == target)
			{
				return ty << 3 | tx;
			}
			if (field[ty][tx] == stopper)
			{
				break;
			}
			ty += dy;
			tx += dx;
		}
		return -1;
	}
	void seekPutable(int row, int col)
	{
		for (int dy = -1; dy <= 1; dy++)
		{
			int y = row + dy;
			if (y < 0 || 8 <= y)
			{
				continue;
			}
			for (int dx = -1; dx <= 1; dx++)
			{
				if (dy == 0 && dx == 0)
				{
					continue;
				}
				int x = col + dx;
				if (x < 0 || 8 <= x)
				{
					continue;
				}
				if (field[y][x] != 3 - turn)
				{
					continue;
				}
				int p = findTarget(y, x, dy, dx, 0, turn);
				if (p >= 0)
				{
					putable[p] = true;
				}
			}
		}
	}
	void seachPutables()
	{
		for (int i = 0; i < 64; i++)
		{
			putable[i] = false;
		}
		for (int row = 0; row < 8; row++)
		{
			for (int col = 0; col < 8; col++)
			{
				if (field[row][col] != turn)
				{
					continue;
				}
				seekPutable(row, col);
			}
		}
	}
	int getFirstSelect()
	{
		for (int i = 0; i < 64; i++)
		{
			if (putable[i])
			{
				return i;
			}
		}
		return -1;
	}
	int getNextSelect(int sel)
	{
		if (sel >= 0)
		{
			for (int i = 1; i <= 64; i++)
			{
				if (putable[(sel + i) & 63])
				{
					return (sel + i) & 63;
				}					
			}
		}
		return -1;
	}
	int getPrevSelect(int sel)
	{
		if (sel >= 0)
		{
			for (int i = 63; i >= 0; i--)
			{
				if (putable[(sel + i) & 63])
				{
					return (sel + i) & 63;
				}					
			}
		}
		return -1;
	}
	
	void drawAllPutables(Graphics g, int sel)
	{
		g.setColor(63, 127, 127);
		for (int i = 0; i < 64; i++)
		{
			if (putable[i] == false)
			{
				continue;
			}
			if (i == sel)
			{
				continue;
			}
			int y = (i >> 3) * 30 + 28;
			int x = (i & 7) * 30;
			g.fillRect(x + 1, y + 1, 28, 28);
		}
		if (sel >= 0)
		{
			g.setColor(127, 255, 255);
			int y = (sel >> 3) * 30 + 28;
			int x = (sel & 7) * 30;
			g.fillRect(x + 1, y + 1, 28, 28);
		}
	}
	
	void clearMass(Graphics g, int row, int col)
	{
		g.setColor(0, 127, 0);
		int y = row * 30 + 28;
		int x = col * 30;
		g.fillRect(x + 1, y + 1, 28, 28);
	}
	
	void drawMass(Graphics g, int row, int col, boolean selected)
	{
		if (selected)
		{
			g.setColor(127, 255, 255);
		}
		else
		{
			g.setColor(63, 127, 127);
		}
		int y = row * 30 + 28;
		int x = col * 30;
		g.fillRect(x + 1, y + 1, 28, 28);
	}
	
	void drawStone(Graphics g, int row, int col, boolean isBlack)
	{
		if (isBlack)
		{
			g.setColor(0, 0, 0);
		}
		else
		{
			g.setColor(255, 255, 255);
		}
		int y = row * 30 + 28;
		int x = col * 30;
		g.fillArc(x + 1, y + 1, 27, 27, 0, 360);
	}
	
	void reverseLineStones(Graphics g, int y, int x, int dy, int dx)
	{
		while (0 <= y && y < 8 && 0 <= x && x < 8)
		{
			if (field[y][x] !=  3 - turn)
			{
				break;
			}
			field[y][x] = turn;
			drawStone(g, y, x, turn == 1);
			y += dy;
			x += dx;
		}
	}
	
	void reverseStones(Graphics g, int sel)
	{
		int row = sel >> 3;
		int col = sel & 7;
		for (int dy = -1; dy <= 1; dy++)
		{
			int y = row + dy;
			if (y < 0 || 8 <= y)
			{
				continue;
			}
			for (int dx = -1; dx <= 1; dx++)
			{
				if (dy == 0 && dx == 0)
				{
					continue;
				}
				int x = col + dx;
				if (x < 0 || 8 <= x)
				{
					continue;
				}
				if (field[y][x] != 3 - turn)
				{
					continue;
				}
				int p = findTarget(y, x, dy, dx, turn, 0);
				if (p < 0)
				{
					continue;
				}
				reverseLineStones(g, y, x, dy, dx);
			}
		}
	}
	
	void putStone(Graphics g, int sel)
	{
		int row = sel >> 3;
		int col = sel & 7;
		drawStone(g, row, col, turn == 1);
		field[row][col] = turn;
		reverseStones(g, sel);
		turn = 3 - turn;
	}
	
	void calcScore()
	{
		black = white = 0;
		for (int row = 0; row < 8; row++)
		{
			for (int col = 0; col < 8; col++)
			{
				switch (field[row][col])
				{
				case 1:
					black++;
					break;
				case 2:
					white++;
					break;
				}
			}
		}
	}
	
	int compute()
	{
		int c = 0;
		for (int i = 0; i < 64; i++)
		{
			if (putable[i])
			{
				c++;
			}
		}
		int sel = rand.nextInt(c);
		boolean down = sel * 2 < c;
		for (int i = 0; i < 64; i++)
		{
			if (putable[i])
			{
				if (sel == 0)
				{
					return down ? i : -i;
				}
				sel--;
			}
		}
		throw new RuntimeException("error compute!");
	}
	
	int getCPUkeyStates(int cpusel, int sel)
	{
		if (Math.abs(cpusel) == sel)
		{
			return FIRE_PRESSED;
		}
		else
		{
			return cpusel < 0 ? UP_PRESSED : DOWN_PRESSED;
		}
	}
	
	public void run()
	{
		Graphics g = getGraphics();
		
		g.setFont(Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_LARGE));
		
		int sel = -1, cpusel = 0;
		for (;;)
		{
			if (flagNewGame)
			{
				initGame();
				drawBase(g);
				drawNames(g);
				drawScores(g);
				drawAllStones(g);
				seachPutables();
				sel = getFirstSelect();
				drawAllPutables(g, sel);
				flushGraphics();
				flagNewGame = false;
			}
			if (sel >= 0)
			{
				int keyStates = getKeyStates();
				if (cpu[turn])
				{
					keyStates = getCPUkeyStates(cpusel, sel);
				}
				if (keyStates == LEFT_PRESSED || keyStates == UP_PRESSED)
				{
					drawMass(g, sel >> 3, sel & 7, false);
					sel = getPrevSelect(sel);
					drawMass(g, sel >> 3, sel & 7, true);
					flushGraphics();
				}
				else if (keyStates == RIGHT_PRESSED || keyStates == DOWN_PRESSED)
				{
					drawMass(g, sel >> 3, sel & 7, false);
					sel = getNextSelect(sel);
					drawMass(g, sel >> 3, sel & 7, true);
					flushGraphics();
				}
				else if (keyStates == FIRE_PRESSED)
				{
					clearPutable(g);
					putStone(g, sel);
					seachPutables();
					sel = getFirstSelect();
					if (sel < 0)
					{
						turn = 3 - turn;
						seachPutables();
						sel = getFirstSelect();
					}
					drawAllPutables(g, sel);
					calcScore();
					drawScores(g);
					flushGraphics();
					if (sel >= 0 && cpu[turn])
					{
						cpusel = compute();
					}
				}
			}
			
			long wait = System.currentTimeMillis() + 300;
			for (long cur = wait - 300; cur < wait; cur = System.currentTimeMillis()) {}
		}
	}
	
	void requestNewGame()
	{
		flagNewGame = true;
	}
}